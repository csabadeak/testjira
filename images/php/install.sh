#!/bin/bash
if [ ! -d /var/www/html/laravel-app ]; then
    composer create-project --prefer-dist laravel/laravel /var/www/html/laravel-app
    cd /var/www/html/laravel-app
    rm /var/www/html/laravel-app/.env
    composer require laravel/passport
    php artisan migrate
    php artisan passport:install
    php artisan db:seed
fi

echo "laravel installed"
php-fpm