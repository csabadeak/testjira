# This is a docker-compose for laravel

## usage

### change database host username / password
change the fallowing values in the docker-compose.yml file:

```
services:
    ...
    php:
        environment:
            DB_DATABASE: laravel
            DB_USERNAME: laravel
            DB_PASSWORD: secret
    ...
    db:
        environment:
            MYSQL_ROOT_PASSWORD: secret
            MYSQL_DATABASE: laravel
            MYSQL_USER: laravel
            MYSQL_PASSWORD: secret
```

### change application path

```
images/nginx/nginx.conf
    root /var/www/html/YOUR-CUSTOM-LOCATION/public;

images/php/install.sh
    if [ ! -d /var/www/html/YOUR-CUSTOM-LOCATION ]; then
        composer create-project --prefer-dist laravel/laravel /var/www/html/YOUR-CUSTOM-LOCATION
        cd /var/www/html/YOUR-CUSTOM-LOCATION
        rm /var/www/html/YOUR-CUSTOM-LOCATION/.env
```

### start

first of all: change your app_key in docker-compose.yml. It will be a 32 char long string
```
services:
    ...
    php:
        ...
        environment:
            APP_KEY: 'place here your 32 char long app key'
```

> docker-compose up

- first it installs laravel
- then creates a laravel application on name laravel-app
- puts all the app files into your ./data/laravel-app directory
- remove the .env file, because all environment value came from the docker-compose.yml




